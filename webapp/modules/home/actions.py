__author__ = 'burchardtist'

from pyramid_handlers import action


class HomeController(object):
    __autoexpose__ = None
    renderer_path = 'modules/home/templates/'

    def __init__(self, request):
        self.request = request

    @action(renderer=renderer_path+'indexView.jinja2', permission='view_acl')
    def index(self):
        return {'text': 'elo! indexview'}
