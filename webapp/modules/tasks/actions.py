__author__ = 'burchardtist'

from pyramid_handlers import action
from webapp.lib.models.tasks import *
from pyramid.httpexceptions import HTTPSeeOther
from webapp.lib.models.task_headers import *
from pyramid.response import Response
from webapp.lib.models.phases import *
from webapp.lib.models.task_phases_headers import *


class TasksController(object):
    __autoexpose__ = None
    renderer_path = 'modules/tasks/templates/'

    def __init__(self, request):
        self.request = request

    @action(renderer=renderer_path+'indexView.jinja2', permission='view_acl')
    def index(self):
        url = self.request.route_url('tasks', action='list')
        return HTTPSeeOther(url)

    @action(renderer=renderer_path+'listView.jinja2', permission='view_acl')
    def list(self):
        return {}

    @action(renderer=renderer_path+'createEditView.jinja2', permission='view_acl')
    def create(self):
        if self.request.method == 'POST':
            pass # todo: dodanie nowego taska do bazy
        return {'task': None}

    @action(renderer=renderer_path+'editTaskView.jinja2', permission='view_acl')
    def edit(self):
        headers = TaskHeaders.selectAll()
        if self.request.params.get('task_id'):
            task = TaskHeaders.selectPK(self.request.params.get('task_id'))
        return {
            'headers': headers,
            'task': locals().get('task', '')
        }

    @action(permission='view_acl')
    def ajaxEditTask(self):
        import json
        if self.request.method == 'POST':
            task_id = self.request.params.get('task_id', None)
            header_id = self.request.params.get('header')
            content = self.request.params.get('content', None)
            creator_id = self.request.user.user_id

            if task_id is None:
                task = Tasks(header_id=header_id, content=content, creator_id=creator_id)
                Tasks.insert(task)
            else:
                task = Tasks.selectPK(task_id)

        return Response(json.dumps({'error': locals().get('error', 0), 'message': locals().get('message', '')}))

    @action(renderer=renderer_path+'tasksSettingsView.jinja2', permission='view_acl')
    def settings(self):
        return {}

    @action(renderer=renderer_path+'todoView.jinja2', permission='view_acl')
    def todo(self):
        return {}

    @action(renderer=renderer_path+'createEditHeaderView.jinja2', permission='view_acl')
    def createEditHeader(self):
        return {}

    @action(permission='view_acl')
    def ajaxCreateEditHeader(self):
        if self.request.method == 'POST':
            import json
            if self.request.POST['action'] == 'create':
                header = self.request.db.query(TaskHeaders).filter_by(name=self.request.POST['name']).first()
                if not header:
                    header = TaskHeaders(name=self.request.POST['name'],
                                         pattern=self.request.POST['pattern'])
                    TaskHeaders.insert(header)
                    return Response(json.dumps({'error': 0, 'message': '^name^ header has been added'}))
                else:
                    return Response(json.dumps({'error': 1, 'message': '^name^ header already exists'}))
            elif self.request.POST['action'] == 'edit':
                session = self.request.db
                try:
                    header_id = self.request.params.get('header_id')
                    name = self.request.params.get('name')
                    pattern = self.request.params.get('pattern')
                    phases = self.request.params.getall('phases')

                    where = ['task_headers_id', header_id, 'eq']
                    header = {
                        'name': name,
                        'pattern': pattern
                     }
                    TaskHeaders.update(where, header)
                    query = self.request.db.query(TaskPhasesHeaders).filter_by(task_headers_id=header_id)
                    for q in TaskPhasesHeaders.selectAll(query):
                        TaskPhasesHeaders.delete(q.tph_id)
                    for id in phases:
                        tph = TaskPhasesHeaders(task_headers_id=header_id,
                                                phase_id=id)
                        TaskPhasesHeaders.insert(tph)
                    return Response(json.dumps({'error': 0,
                                                'message': '^name^ has been edited',
                                                'action': 'edit',
                                                'header_id': header_id}))
                except:
                    session.rollback()
                    return Response({'error': 1, 'message': 'error'})
            elif self.request.POST['action'] == 'remove':
                header_id = self.request.POST['header_id']
                try:
                    import datetime
                    self.request.db.query(TaskHeaders).filter_by(task_headers_id=header_id).update({'is_deleted': True,
                                                                                                    'date_deleted': datetime.datetime.now()})
                    return Response(json.dumps({'error': 0,
                                                'message': '^name^ has been deleted',
                                                'action': 'remove',
                                                'header_id': header_id}))
                except Exception as e:
                    print(e)
                    return Response(json.dumps({'error': 1, 'message': 'error'}))


    @action(renderer=renderer_path+'showHeaderView.jinja2', permission='view_acl')
    def showHeader(self):
        headers = TaskHeaders.selectAll()
        phases = self.request.db.query(Phases).all()
        return {'headers': headers,
                'phases': phases}

    @action(renderer=renderer_path+'createEditPhaseView.jinja2', permission='view_acl')
    def createEditPhase(self):
        return {}

    @action(permission='view_acl')
    def ajaxCreateEditPhase(self):
        if self.request.method == 'POST':
            import json
            phase = self.request.db.query(Phases).filter_by(name=self.request.POST['name']).first()
            if not phase:
                phase = Phases(name=self.request.POST['name'])
                Phases.insert(phase)
                return Response(json.dumps({'error': 0, 'message': '^name^ phase has been added'}))
            else:
                return Response(json.dumps({'error': 1, 'message': '^name^ phase already exists'}))

    @action(renderer=renderer_path+'editDeletePhaseView.jinja2', permission='view_acl')
    def editDeletePhase(self):
        phases = self.request.db.query(Phases).all()
        return {'phases': phases}

    @action(permission='view_acl')
    def ajaxRenamePhase(self):
        import json
        phase = self.request.db.query(Phases).filter_by(name=self.request.POST['name']).first()
        if not phase:
            phase = self.request.db.query(Phases).filter_by(phase_id=self.request.POST['phase_id']).update({"name": self.request.POST['name']})
            return Response(json.dumps({'error': 0,
                                        'message': 'Phase has been renamed',
                                         'phase_id': self.request.POST['phase_id'],
                                         'phase_name': self.request.POST['name']}))
        else:
            return Response(json.dumps({'error': 1, 'message': 'phase with this name already exists'}))

    @action(permission='view_acl')
    def ajaxGetHeaderPhases(self):
        if self.request.method == 'POST':
            import json
            phases = self.request.db.query(TaskPhasesHeaders).filter_by(task_headers_id=self.request.POST['task_headers_id'])
            phases = phases.all()
            header_phases = []
            for phase in phases:
                header_phases.append(phase.phase_id)
            return Response(json.dumps({
                'header_phases': header_phases
            }))
