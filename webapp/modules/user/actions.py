from webapp.lib.models.user_account import UserAccount
from pyramid.httpexceptions import HTTPSeeOther, HTTPFound
from pyramid_handlers import action
import hashlib
from pyramid.security import remember, forget
from webapp.lib.config import SALT
from sqlalchemy.exc import IntegrityError
__author__ = 'burchardtist'


class UserController(object):
    __autoexpose__ = None
    renderer_path = 'modules/user/templates/'

    def __init__(self, request):
        self.request = request

    @action()
    def index(self):
        url = self.request.route_url('user', action='login')
        return HTTPSeeOther(url)

    @action(renderer=renderer_path + 'loginView.jinja2', permission='login_acl')
    def login(self):
        if self.request.method == 'POST':
            user_post = {
                'email': self.request.POST['email'],
                'password': hashlib.sha256((self.request.POST['email']+self.request.POST['password']+SALT).encode()).hexdigest(),
                'remember_me': True if 'remember_me' in self.request.POST else False,  # todo: remember_me dla cookies
            }
            session = self.request.db
            user = session.query(UserAccount).filter_by(email=user_post['email'], password=user_post['password']).first()
            if user:
                login_url = self.request.route_url('user', action='login')
                referrer = self.request.url
                if referrer == login_url:
                    referrer = '/'
                came_from = self.request.params.get('came_from', referrer)
                headers = remember(self.request, user.user_id)
                return HTTPFound(location=came_from, headers=headers)
            else:
                message = 'Invalid email or password'
                return {'message': message}
        return {}

    @action()
    def logout(self):
        headers = forget(self.request)
        url = self.request.route_url('home_index')
        return HTTPFound(location=url,
                         headers=headers)

    @action(renderer=renderer_path + 'registerView.jinja2', permission='login_acl')
    def register(self):
        if self.request.method == 'POST':
            user_post = {
                'email': self.request.POST['email'],
                'password': hashlib.sha256((self.request.POST['email']+self.request.POST['password']+SALT).encode()).hexdigest(),
                'password_confirm': hashlib.sha256((self.request.POST['email']+self.request.POST['password_confirm']+SALT).encode()).hexdigest(),
                'username': self.request.POST['username'],
            }
            result = self._new_user_rollback_or_add(user_post)
            return {'errors': result}
        return {}

    def _new_user_rollback_or_add(self, user_post):
        session = self.request.db
        try:
            errors = {}
            email_integrity = session.query(UserAccount).filter_by(email=user_post['email']).first()

            if email_integrity:
                errors.update({'email_error': user_post['email']})

            username_integrity = session.query(UserAccount).filter_by(username=user_post['username']).first()
            if username_integrity:
                errors.update({'username_error': user_post['username']})

            if user_post['password'] != user_post['password_confirm']:
                errors.update({'password_error': ''})
            if bool(errors):
                return errors

            new_user = UserAccount(email=user_post['email'],
                                   password=user_post['password'],
                                   username=user_post['username'],
                                   )
            UserAccount.insert(new_user)
            session.flush()
            return HTTPFound(location='/')
        except IntegrityError:
            session.rollback()
            return {}
