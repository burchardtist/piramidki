def regex_replace(s, pattern, group=1):
    import re
    m = re.search(pattern, s)
    if m:
        return m.group(group)
    return ''
