
from webapp.lib.models import Base, DBSession
from webapp.lib.models.model_base import ModelBase
from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Integer
from sqlalchemy import ForeignKey


class TaskPhasesHeaders(Base, ModelBase):
    __tablename__ = 'task_phases_headers'

    tph_id = Column('tph_id', Integer, primary_key=True)
    task_headers_id = Column('task_headers_id', Integer, ForeignKey("task_headers.task_headers_id"), nullable=False)
    phase_id = Column('phase_id', Integer, ForeignKey("phases.phase_id"), nullable=False)

    def __init__(self, **kwargs):
        self.tph_id = kwargs.get('tph_id', None)
        self.task_headers_id = kwargs.get('task_headers_id', None)
        self.phase_id = kwargs.get('phase_id', None)

    def __repr__(self):
        return '<TaskPhasesHeaders tph_id={}, task_headers_id={}, phase_id={}>'\
        .format(self.tph_id, self.task_headers_id, self.phase_id)
