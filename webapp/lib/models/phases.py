from webapp.lib.models import Base
from webapp.lib.models.model_base import ModelBase
from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Integer

class Phases(Base, ModelBase):
    __tablename__ = 'phases'

    phase_id = Column('phase_id', Integer, primary_key=True)
    name = Column('name', String(255), nullable=False)

    def __init__(self, **kwargs):
        self.name = kwargs.get('name', None)

    def __repr__(self):
        return '<Phase phase_id={}, name={}>'.format(self.phase_id, self.name)
