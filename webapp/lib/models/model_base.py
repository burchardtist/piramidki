from sqlalchemy.ext.declarative import declared_attr
from webapp.lib.models import DBSession
from sqlalchemy.inspection import inspect

class ModelBase():

    @classmethod
    def selectAll(cls, criteria=None):
        deleted_exists = None
        if criteria is None:
            criteria = DBSession.query(cls)
        if '_criterion' in criteria.__dict__:
            criterion = criteria.__dict__['_criterion']
            deleted_exists = str(criterion).find('{}.is_deleted'.format(cls.__tablename__))
        else:
            criterion = {}
        if deleted_exists in (-1, None):
            try:
                criteria = criteria.filter(cls.is_deleted==False)
            except AttributeError:
                pass
        return criteria.all()

    @classmethod
    def selectPK(cls, pk):
        criteria = DBSession.query(cls).from_self()
        criteria = criteria.filter(inspect(cls).primary_key[0]==pk)
        criteria = criteria.limit(1).from_self()
        return cls.selectAll(criteria)

    @classmethod
    def update(cls, where, set_):
        if type(set_) is not dict or type(where) is not list:
            raise TypeError('where and set_ must has a proper type')
        if type(where[0]) != list:
            where = [where]
        session = DBSession.query(cls)
        filt = ''
        for key, value, op in where:
            column = getattr(cls, key, None)
            if op == 'in':
                if isinstance(value, list):
                    filt = column.in_(value)
                else:
                    filt = column.in_(value.split(','))
            else:
                try:
                    attr = list(filter(
                        lambda e: hasattr(column, e % op),
                        ['%s', '%s_', '__%s__']
                    ))[0] % op
                except IndexError:
                    raise Exception('Invalid filter operator: %s' % op)
                if value == 'null':
                    value = None
                filt = getattr(column, attr)(value)
            session = session.filter(filt)
        session.update(set_)

    @classmethod
    def delete(cls, pk):
        primary_key = inspect(cls).primary_key[0]
        if hasattr(cls, 'is_deleted'):
            where = [primary_key.name, pk, 'eq']
            set_ = {cls.is_deleted: True}
            cls.update(where, set_)
        else:
            session = DBSession.query(cls)
            session.filter(primary_key==pk)
            session.delete()
            DBSession.commit()

    @classmethod
    def insert(cls, ob):
        try:
            if 'is_deleted' in ob.__dict__ and not ob.__dict__['is_deleted']:
                ob.is_deleted = False
            for column in cls.__table__.columns:
                if column.nullable == False\
                and column.primary_key == False\
                and ob.__dict__[str(column.name)] in [None, '']:
                    raise AttributeError('not null error: {}'.format(column.__dict__['name']))
            DBSession.add(ob)
            DBSession.commit()
            return True
        except AttributeError as e:
            print(e)
            raise e
