from webapp.lib.models import Base
from webapp.lib.models import DBSession
from webapp.lib.models.model_base import ModelBase
from webapp.lib.helpers.helper import *
from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Integer
from sqlalchemy import DateTime
from sqlalchemy import Boolean
import datetime

class UserAccount(Base, ModelBase):
    __tablename__ = 'user_account'

    user_id = Column('user_id', Integer, primary_key=True)
    email = Column('email', String(50), nullable=False, unique=True)
    password = Column('password', String(64), nullable=False)
    username = Column('username', String(50), nullable=False, unique=True)
    register_date = Column('register_date', DateTime)
    last_login = Column('last_login', DateTime, default=datetime.datetime.now)
    is_deleted = Column('is_deleted', Boolean, default=False)

    def __init__(self, **kwargs):
        if not isInt(kwargs.get('email')) \
        or not isInt(kwargs.get('username')) \
        or not isInt(kwargs.get('password')):
            raise TypeError('email, username, password must be type of int')
        self.email = kwargs.get('email')
        self.password = kwargs.get('password')
        self.username = kwargs.get('username')
