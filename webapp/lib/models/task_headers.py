from webapp.lib.models import Base, DBSession
from webapp.lib.models.model_base import ModelBase
from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Integer
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import Boolean
import datetime


class TaskHeaders(Base, ModelBase):
    __tablename__ = 'task_headers'

    task_headers_id = Column('task_headers_id', Integer, primary_key=True)
    name = Column('name', String(255), unique=True, nullable=False)
    pattern = Column('pattern', String(255), nullable=False)
    is_deleted = Column('is_deleted', Boolean, default=False, nullable=False)
    date_deleted = Column('date_deleted', DateTime)

    def __init__(self, **kwargs):
        self.task_headers_id = kwargs.get('task_headers_id', None)
        self.name = kwargs.get('name', None)
        self.pattern = kwargs.get('pattern', None)
        self.is_deleted = kwargs.get('is_deleted', None)
        self.date_deleted = kwargs.get('date_deleted', None)
