from webapp.lib.models import Base
from webapp.lib.models import DBSession
from webapp.lib.models.model_base import ModelBase
from webapp.lib.helpers.helper import *
from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Integer
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import Boolean
import datetime

class Tasks(Base, ModelBase):
    __tablename__ = 'tasks'

    task_id = Column('task_id', Integer, primary_key=True)
    creator_id = Column('creator_id', Integer, ForeignKey("user_account.user_id"), nullable=False)
    header_id = Column('header_id', Integer, ForeignKey("task_headers.task_headers_id"), nullable=False)
    content = Column('content', String)
    phase_id = Column('phase_id', Integer, ForeignKey("phases.phase_id"))
    created_at = Column('created_at', DateTime, default=datetime.datetime.now)
    updated_at = Column('updated_at', DateTime)
    is_deleted = Column('is_deleted', Boolean, default=False)

    def __init__(self, **kwargs):
        if not isInt(kwargs.get('creator_id')) \
        or not isInt(kwargs.get('header_id')):
            raise TypeError('creator_id and header_id must be type of int')
        self.creator_id = kwargs.get('creator_id')
        self.header_id = kwargs.get('header_id')
        self.content = kwargs.get('content', None)
        self.phase_id = kwargs.get('phase_id', None)
        self.updated_at = None
