from pyramid.httpexceptions import HTTPSeeOther
from pyramid.view import forbidden_view_config
from webapp.lib.models.user_account import UserAccount
__author__ = 'burchardtist'


@forbidden_view_config()
def forbidden_view(request):
    if request.authenticated_userid:
        return request.context
    url = request.route_url('user', action='login')
    return HTTPSeeOther(url)

def get_user(request):
    session = request.db
    user_id = request.authenticated_userid
    return UserAccount.selectPK(user_id)[0]
