from pyramid.security import Allow, Everyone, Authenticated
__author__ = 'burchardtist'

class RootFactory(object):
    __acl__ = [
        (Allow, Everyone, 'login_acl'),
        (Allow, Authenticated, 'view_acl'),
    ]

    def __init__(self, request):
        self.request = request
