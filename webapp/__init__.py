from webapp.modules.user.actions import UserController
from pyramid.config import Configurator
from webapp.modules.home.actions import HomeController
from webapp.modules.tasks.actions import TasksController
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from webapp.lib.config import RootFactory, AuthenticationConfig
from sqlalchemy.orm import sessionmaker
from sqlalchemy import engine_from_config
from webapp.lib.models import initialize_sql

def main(global_config, **settings):
    config = Configurator(settings=settings)

    #session
    authn_policy = AuthTktAuthenticationPolicy('ffc2f86dcb977b6ecd6b813747bbbd03', hashalg='sha512')
    authz_policy = ACLAuthorizationPolicy()
    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(authz_policy)
    config.set_default_permission('view_acl')

    config.set_root_factory(RootFactory.RootFactory)

    #forbidden view
    config.add_forbidden_view(AuthenticationConfig.forbidden_view)

    #view routes
    config.add_handler('user', '/user/{action}', handler=UserController)
    config.add_handler('user_index', '/user', handler=UserController, action='index')
    config.add_handler('tasks', '/tasks/{action}', handler=TasksController)
    config.add_handler('tasks_index', '/tasks', handler=TasksController, action='index')
    config.add_handler('home', '/{action}', handler=HomeController)
    config.add_handler('home_index', '/', handler=HomeController, action='index')  # HomeController MUSI byc ostatni

    #static view
    config.add_static_view(name='static', path='webapp:static')

    #sqlalchemy db
    engine = engine_from_config(settings, prefix='sqlalchemy.')
    initialize_sql(engine)
    config.registry.dbmaker = sessionmaker(bind=engine)
    config.add_request_method(db, reify=True)

    config.add_request_method(AuthenticationConfig.get_user, 'user', reify=True)

    #return config
    return config.make_wsgi_app()


def db(request):
    maker = request.registry.dbmaker
    session = maker()

    def cleanup(request):
        if request.exception is not None:
            session.rollback()
        else:
            session.commit()
        session.close()
    request.add_finished_callback(cleanup)

    return session
