from setuptools import setup

requires = [
    'pyramid',
    'pyramid_jinja2',
    'pyramid_debugtoolbar',
    'pyramid_handlers',
    'zope.sqlalchemy',
    'pyramid_tm',
    'psycopg2',
]

setup(name='webapp',
      install_requires=requires,
      version='0.1.0',
      author='Aleksander Philips',
      author_email='aleksander.philips@gmail.com',
      entry_points="""\
      [paste.app_factory]
      main = webapp:main
      """,
)
