ALTER TABLE task_headers
ADD COLUMN is_deleted boolean default(false) NOT NULL;

ALTER TABLE task_headers
ADD COLUMN date_deleted timestamp without time zone;
