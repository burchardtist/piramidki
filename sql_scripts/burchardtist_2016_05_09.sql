CREATE TABLE task_headers(
  task_headers_id bigserial primary key,
  name varchar(255) NOT NULL,
  pattern varchar(255) NOT NULL
);

CREATE TABLE task_phases_headers(
  tph_id bigserial primary key,
  task_headers_id int NOT NULL,
  phase_id int NOT NULL,
  CONSTRAINT fk_task_headers_id FOREIGN KEY (task_headers_id) REFERENCES task_headers(task_headers_id),
  CONSTRAINT fk_phase_id FOREIGN KEY (phase_id) REFERENCES phases(phase_id)
);
