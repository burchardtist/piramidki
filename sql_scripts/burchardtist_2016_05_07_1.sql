ALTER TABLE user_account
ALTER COLUMN register_date SET DEFAULT (date_trunc('seconds', now()::timestamp) at time zone 'cest');

ALTER TABLE tasks
ALTER COLUMN create_date SET default (date_trunc('seconds', now()::timestamp) at time zone 'cest');
