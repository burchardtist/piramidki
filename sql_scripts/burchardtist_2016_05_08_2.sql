UPDATE tasks
SET phase_id = (SELECT phase_id FROM phases WHERE name='todo')
WHERE phase_id is NULL;

ALTER TABLE tasks
ALTER COLUMN phase_id SET NOT NULL;
