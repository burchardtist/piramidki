CREATE TABLE phases(
  phase_id bigserial primary key,
  name varchar(255) NOT NULL
);

ALTER TABLE tasks
DROP COLUMN phase;

ALTER TABLE tasks
ADD COLUMN phase_id int;

ALTER TABLE tasks
ALTER COLUMN phase_id set DEFAULT(0);

ALTER TABLE tasks
ADD CONSTRAINT fk_phase_id FOREIGN KEY (phase_id) REFERENCES phases(phase_id);
