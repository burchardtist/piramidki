ALTER TABLE tasks
ALTER COLUMN phase_id DROP DEFAULT;

INSERT INTO phases(name)
VALUES ('todo'), ('during execution'), ('finished');
