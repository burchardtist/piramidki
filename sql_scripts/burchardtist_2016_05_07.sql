CREATE TABLE tasks(
  task_id bigserial primary key,
  creator_id int NOT NULL,
  header varchar(255) NOT NULL,
  content text,
  phase int NOT NULL DEFAULT(0),
  create_date timestamp without time zone default (now() at time zone 'cest'),
  CONSTRAINT fk_user_id FOREIGN KEY (creator_id) REFERENCES user_account(user_id)
);
