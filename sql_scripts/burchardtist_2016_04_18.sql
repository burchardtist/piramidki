DROP TABLE user_account;

CREATE TABLE user_account(
    user_id bigserial primary key,
    email varchar(50) NOT NULL UNIQUE,
    password char(64) NOT NULL,
    username varchar(50) NOT NULL UNIQUE,
    register_date timestamp without time zone default (now() at time zone 'cest'),
    last_login timestamp without time zone
);