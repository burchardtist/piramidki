ALTER TABLE user_account
ADD COLUMN is_deleted boolean default false;
