DROP TABLE tasks;

CREATE TABLE tasks(
  task_id bigserial primary key,
  creator_id int NOT NULL,
  header_id int NOT NULL,
  content text,
  phase_id int,
  created_at timestamp without time zone default (now() at time zone 'cest'),
  updated_at timestamp without time zone,
  CONSTRAINT fk_user_id FOREIGN KEY (creator_id) REFERENCES user_account(user_id),
  CONSTRAINT fk_header_id FOREIGN KEY (header_id) REFERENCES task_headers(task_headers_id),
  CONSTRAINT fk_phase_id FOREIGN KEY (phase_id) REFERENCES phases(phase_id)
);
